local assets =
{
	Asset( "ANIM", "anim/lai.zip" ),
	Asset( "ANIM", "anim/ghost_lai_build.zip" ),
}

local skins =
{
	normal_skin = "lai",
	ghost_skin = "ghost_lai_build",
}

return CreatePrefabSkin("lai_none",
{
	base_prefab = "lai",
	type = "base",
	assets = assets,
	skins = skins, 
	skin_tags = {"LAI", "CHARACTER", "BASE"},
	build_name_override = "lai",
	rarity = "Character",
})