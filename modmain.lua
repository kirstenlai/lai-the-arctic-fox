PrefabFiles = {"lai", "lai_none"}

Assets = {
    Asset("IMAGE", "images/saveslot_portraits/lai.tex"),
    Asset("ATLAS", "images/saveslot_portraits/lai.xml"),

    Asset("IMAGE", "images/selectscreen_portraits/lai.tex"),
    Asset("ATLAS", "images/selectscreen_portraits/lai.xml"),

    Asset("IMAGE", "images/selectscreen_portraits/lai_silho.tex"),
    Asset("ATLAS", "images/selectscreen_portraits/lai_silho.xml"),

    Asset("IMAGE", "bigportraits/lai.tex"),
    Asset("ATLAS", "bigportraits/lai.xml"),

    Asset("IMAGE", "images/map_icons/lai.tex"),
    Asset("ATLAS", "images/map_icons/lai.xml"),

    Asset("IMAGE", "images/avatars/avatar_lai.tex"),
    Asset("ATLAS", "images/avatars/avatar_lai.xml"),

    Asset("IMAGE", "images/avatars/avatar_ghost_lai.tex"),
    Asset("ATLAS", "images/avatars/avatar_ghost_lai.xml"),

    Asset("IMAGE", "images/avatars/self_inspect_lai.tex"),
    Asset("ATLAS", "images/avatars/self_inspect_lai.xml"),

    Asset("IMAGE", "images/names_lai.tex"),
    Asset("ATLAS", "images/names_lai.xml"),

    Asset("IMAGE", "images/names_gold_lai.tex"),
    Asset("ATLAS", "images/names_gold_lai.xml"),

    Asset("ANIM", "anim/lai_meter.zip")
}

AddMinimapAtlas("images/map_icons/lai.xml")

local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS

-- The character select screen lines
STRINGS.CHARACTER_TITLES.lai = "The Arctic Fox"
STRINGS.CHARACTER_NAMES.lai = "Lai"
STRINGS.CHARACTER_DESCRIPTIONS.lai =
    "*It takes considerably longer to get cold in winter\n*It's a little faster than the others\n*regenerates his own life little by little"
STRINGS.CHARACTER_QUOTES.lai = "\"Quote\""

-- Custom speech strings
STRINGS.CHARACTERS.LAI = require "speech_lai"

-- The character's name as appears in-game 
STRINGS.NAMES.LAI = "Lai"
STRINGS.SKIN_NAMES.lai_none = "Lai"

-- Add mod character to mod character list. Also specify a gender. Possible genders are MALE, FEMALE, ROBOT, NEUTRAL, and PLURAL.
AddModCharacter("lai", "FEMALE")
